﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SportsStore.API.Controllers;
using SportsStore.Domain.Entities;

namespace SportsStore.API.Infrastructure
{
    public static class Extensions
    {
        public static IEnumerable<Product> Page(this IEnumerable<Product> products, int pageSize, int page)
        {
            return products
                .OrderBy(x => x.ProductID)
                .Skip((page - 1)*pageSize)
                .Take(pageSize);
        }
    }
}