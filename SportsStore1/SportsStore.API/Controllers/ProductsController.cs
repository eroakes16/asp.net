﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SportsStore.Domain;
using SportsStore.Domain;
using SportsStore.Domain.Entities;
using SportsStore.API.Infrastructure;

namespace SportsStore.API.Controllers
{
    public class ProductsController : ApiController
    {
        private IEnumerable<Product> Products
        {
            get
            {
                return new List<Product>
                {
                    new Product
                    {
                        ProductID = 1,
                        Name = "Jacket",
                        Description = "Fuzzy and warm",
                        Category = "Clothing",
                        Price = 150M
                    },
                    new Product
                    {
                        ProductID = 2,
                        Name = "Snowboard",
                        Description = "Fast",
                        Category = "Equipment",
                        Price = 300M
                    },
                    new Product
                    {
                        ProductID = 3,
                        Name = "Swimsuit",
                        Description = "For swimming...",
                        Category = "Clothing",
                        Price = 25M
                    }

                };
            }
        }
        public IEnumerable<Product> Get(string category = "", int pageSize = 3, int page = 1)
        {
            return string.IsNullOrWhiteSpace(category)
                ? Products.Page(pageSize, page)
                : Products
                .Where(product => product.Category.ToLower() == category.ToLower())
                .Page(pageSize, page);
        }
    }
}
